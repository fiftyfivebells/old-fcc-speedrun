'use strict';

var streamers = ['ESL_SC2', 'OgamingSC2', 'cretetion', 'freecodecamp', 'habathcx', 'RobotCaleb', 'noobs2ninjas', 'brunofin'];

function getUser(channel, type) {
    return new Promise((resolve, reject) => {
        let http = new XMLHttpRequest(),
            url = `https://wind-bow.glitch.me/twitch-api/${type}/${channel}`;
        http.open('GET', url);
        http.responseType = 'json';
        http.onreadystatechange = () => {
            if (http.readyState == 4 && http.status == 200) {
                resolve(http.response);
            }
        }
        http.send();
    });
};

function getStreams() {
    let ul = document.getElementById('results');
    streamers.forEach(strmr => {
        Promise.all([getUser(strmr, 'channels'), getUser(strmr, 'streams')])
            .then(data => {

                console.log( data[0], data[1]);
                if (data[0].status == 404) {
                    console.log( "made it");
                    ul.innerHTML += `<li class="entries visible offline"><img class='img' src='https://pixy.org/images/placeholder.png'>
                <div class='str-name'>${strmr}</div>
                <div class='str-info'>User is offline</div>
                </li>`;
                }

                else if (data[1].stream == null) {
                    ul.innerHTML += `<li class="entries visible offline"><img class='img' src='${data[0].logo}'>
                <div class='str-name'>${data[0].display_name}</div>
                <div class='str-info'>${data[0].status}</div>
                </li>`;
                }
                else {
                    ul.innerHTML += `<li class="entries visible online"><img class='img' src='${data[0].logo}'>
                <div class='str-name'>${data[0].display_name}</div>
                <div class='str-info'>${data[0].status}</div>
                </li>`;
                }
            })
    })
}

document.addEventListener('DOMContentLoaded', function () {
    let online = document.getElementById('online'),
        offline = document.getElementById('offline'),
        all = document.getElementById('all'),
        onArr = document.getElementsByClassName('online'),
        offArr = document.getElementsByClassName('offline');

    getStreams();

    online.addEventListener('click', () => {
        for (let el of offArr) {
            el.classList.remove('visible');
            el.classList.add('hidden');
        }
        for (let el of onArr) {
            el.classList.remove('hidden');
            el.classList.add('visible');
        }
    });

    offline.addEventListener('click', () => {
        for (let el of onArr) {
            el.classList.remove('visible');
            el.classList.add('hidden');
        }
        for (let el of offArr) {
            el.classList.remove('hidden');
            el.classList.add('visible');
        }
    });

    all.addEventListener('click', () => {
        for (let el of offArr) {
            el.classList.remove('hidden');
            el.classList.add('visible');
        }
        for (let el of onArr) {
            el.classList.remove('hidden');
            el.classList.add('visible');
        }
    });

});
