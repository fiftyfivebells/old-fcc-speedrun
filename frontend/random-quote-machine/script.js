let colors = ['orange', 'cadetblue', 'darkgoldenrod', 'darkseagreen', 'darkturquoise', 'firebrick', 'chocoloate', 'deeppink'];

function changeBG() {
    let random = Math.floor(Math.random() * colors.length);

    document.body.style.background = colors[random];
    document.getElementById('quote-button').style.background = colors[random];
    document.getElementsByClassName('box')[0].style.color = colors[random];
}

function getQuote() {
    return new Promise((resolve, reject) => {
        let http = new XMLHttpRequest(),
            url = 'https://random-quote-generator.herokuapp.com/api/quotes/random';

        http.open('GET', url);
        http.responseType = 'json';

        http.onload = () => {

            if (http.status == 200) {
                resolve(http.response);
            }
            else {
                reject(http.statusText);
            }
        };
        http.send();
    });
};

document.addEventListener('DOMContentLoaded', () => {
    changeBG();
    getQuote()
        .then(data => {
            changeBG();
            document.getElementById('quote').innerHTML = data.quote;
            document.getElementById('author').innerHTML = `-- ${data.author}`;
        });

    document.getElementById('quote-button').addEventListener('click', () => {
        getQuote()
            .then(data => {
                document.getElementById('quote').innerHTML = data.quote;
                document.getElementById('author').innerHTML = `-- ${data.author}`;
            })
            .then(() => {
                changeBG();
            });
    });
});
