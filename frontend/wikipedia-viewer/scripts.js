function randomButton() {
    'use strict';
    let names = ["Roll the Dice", "Pick a Card", "Spin the Wheel", "Feeling Lucky?", "Take a Chance", "Leap of Faith"], 
	random = Math.floor(Math.random() * names.length),
	buttonName = names[random];
    
	document.getElementById('random').innerHTML = buttonName;
}

function wikiSearch() {
    'use strict';
    let data,
		cache = {},
		ul = document.getElementById('results'),
        term = document.getElementById('bar').value,
        wiki = new XMLHttpRequest(),
        url = 'https://en.wikipedia.org/w/api.php?format=json&action=query&origin=*&generator=search&gsrnamespace=0&gsrlimit=10&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&gsrsearch=' + term;
	
	// make sure that the list is empty before we start edding stuff 
	ul.innerHTML = '';
    
    wiki.onreadystatechange = function () {
        if(wiki.readyState == 4 && wiki.status == 200) {
            data = JSON.parse(wiki.response);
			let sresult = Object.values(data.query.pages);
			
			if (sresult == undefined) {
				ul.innerHTML += "<li class='entry'>Your search return no entries.</li>";
			} else {
		
				for (let i = 0; i < sresult.length; i++) {
					ul.innerHTML += `<a href='https://en.wikipedia.org/?curid=${sresult[i].pageid}'><li class='entry'>${sresult[i].title}: </br></br> ${sresult[i].extract}</li></a>`;
					
				};
			}
        }
    }
	
	// remove search term and focus from the text bar
	document.getElementById('bar').value = '';
	document.getElementById('bar').blur();
	
	// open and get data from URL
    wiki.open("GET", url, true);
    wiki.send();
};

function searchEnter() {
    'use strict';
    let key = window.event.keyCode;

    // make sure it was the enter key pressed
    if (key == 13) {
	
		wikiSearch();
    }
}

function randomSearch() {
    'use strict'
    window.open("https://en.wikipedia.org/wiki/Special:Random", '_self');
}


window.onload = function () {
    'use strict';
	randomButton();
	
	// random search
    document.getElementById('random').onclick = randomButton;
    document.getElementById('random').onclick = randomSearch;
	
    // specific search
    document.getElementById('bar').keypress = searchEnter;
	document.getElementById('wiki-search').onclick = wikiSearch;
    
}


