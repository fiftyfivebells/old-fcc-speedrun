var latitude,
    longitude,
    celsius = false,
    key = 'cc39a83131a444229ac212528171103',
    forecast = `https://api.apixu.com/v1/forecast.json?key=${key}&q=Paris`,
    current= `https://api.apixu.com/v1/current.json?key=${key}&q=Paris`;

function getCoords() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(resolve, reject);
    });
}
   

function weather(url) {
    return new Promise((resolve, reject) => {
        let xhttp = new XMLHttpRequest();

            xhttp.open('GET', url, true);
            xhttp.responseType = 'json';
            xhttp.onload = () =>{
                if (xhttp.status == 200) {
                    resolve(xhttp.response);
                }
                else {
                    reject(xhttp.statusText);
                }
            }            
            xhttp.send();
    });
}

document.addEventListener( 'DOMContentLoaded', () => {
    getCoords()
    .then( data => {
        latitude = data.coords.latitude;
        longitude = data.coords.longitude;
        forecast = `https://api.apixu.com/v1/forecast.json?key=${key}&q=${latitude},${longitude}&days=5`; 
    })
    .then(() => weather(forecast))
    .then( data => {
        // current weather
        document.getElementById('location').innerHTML = data.location.name;
        document.getElementById('condition').innerHTML = `<img src='${data.current.condition.icon}'>`;
        document.getElementById('temp').innerHTML = `${data.current.temp_f}${String.fromCharCode(176)} F`;
        document.getElementById('cond-text').innerHTML = data.current.condition.text;

        // day 1 forecast
        document.getElementById('img-one').innerHTML = `<img src='${data.forecast.forecastday[1].day.condition.icon}'>`;
        document.getElementById('wth-one').innerHTML = data.forecast.forecastday[1].day.condition.text;
        document.getElementById('temp-one').innerHTML = `${data.forecast.forecastday[1].day.avgtemp_f}${String.fromCharCode(176)} F`;

        // day 2 forecast
        document.getElementById('img-two').innerHTML = `<img src='${data.forecast.forecastday[2].day.condition.icon}'>`;
        document.getElementById('wth-two').innerHTML = data.forecast.forecastday[2].day.condition.text;
        document.getElementById('temp-two').innerHTML = `${data.forecast.forecastday[2].day.avgtemp_f}${String.fromCharCode(176)} F`;

        // day 3 forecast
        document.getElementById('img-thr').innerHTML = `<img src='${data.forecast.forecastday[3].day.condition.icon}'>`;
        document.getElementById('wth-thr').innerHTML = data.forecast.forecastday[3].day.condition.text;
        document.getElementById('temp-thr').innerHTML = `${data.forecast.forecastday[3].day.avgtemp_f}${String.fromCharCode(176)} F`;


    });

    document.getElementById('swap').addEventListener('click', () => {
        if (!celsius) {
            celsius = true;
            weather(forecast)
            .then( data => {
            // current weather
            document.getElementById('location').innerHTML = data.location.name;
            document.getElementById('condition').innerHTML = `<img src='${data.current.condition.icon}'>`;
            document.getElementById('temp').innerHTML = `${data.current.temp_c}${String.fromCharCode(176)} C`;
            document.getElementById('cond-text').innerHTML = data.current.condition.text;

            // day 1 forecast
            document.getElementById('img-one').innerHTML = `<img src='${data.forecast.forecastday[1].day.condition.icon}'>`;
            document.getElementById('wth-one').innerHTML = data.forecast.forecastday[1].day.condition.text;
            document.getElementById('temp-one').innerHTML = `${data.forecast.forecastday[1].day.avgtemp_c}${String.fromCharCode(176)} F`;

            // day 2 forecast
            document.getElementById('img-two').innerHTML = `<img src='${data.forecast.forecastday[2].day.condition.icon}'>`;
            document.getElementById('wth-two').innerHTML = data.forecast.forecastday[2].day.condition.text;
            document.getElementById('temp-two').innerHTML = `${data.forecast.forecastday[2].day.avgtemp_c}${String.fromCharCode(176)} C`;

            // day 3 forecast
            document.getElementById('img-thr').innerHTML = `<img src='${data.forecast.forecastday[3].day.condition.icon}'>`;
            document.getElementById('wth-thr').innerHTML = data.forecast.forecastday[3].day.condition.text;
            document.getElementById('temp-thr').innerHTML = `${data.forecast.forecastday[3].day.avgtemp_c}${String.fromCharCode(176)} C`;
                })
        }
        else {
            celsius = false;
            weather(forecast)
            .then( data => {
            // current weather
            document.getElementById('location').innerHTML = data.location.name;
            document.getElementById('condition').innerHTML = `<img src='${data.current.condition.icon}'>`;
            document.getElementById('temp').innerHTML = `${data.current.temp_f}${String.fromCharCode(176)} F`;
            document.getElementById('cond-text').innerHTML = data.current.condition.text;

            // day 1 forecast
            document.getElementById('img-one').innerHTML = `<img src='${data.forecast.forecastday[1].day.condition.icon}'>`;
            document.getElementById('wth-one').innerHTML = data.forecast.forecastday[1].day.condition.text;
            document.getElementById('temp-one').innerHTML = `${data.forecast.forecastday[1].day.avgtemp_f}${String.fromCharCode(176)} F`;

            // day 2 forecast
            document.getElementById('img-two').innerHTML = `<img src='${data.forecast.forecastday[2].day.condition.icon}'>`;
            document.getElementById('wth-two').innerHTML = data.forecast.forecastday[2].day.condition.text;
            document.getElementById('temp-two').innerHTML = `${data.forecast.forecastday[2].day.avgtemp_f}${String.fromCharCode(176)} F`;

            // day 3 forecast
            document.getElementById('img-thr').innerHTML = `<img src='${data.forecast.forecastday[3].day.condition.icon}'>`;
            document.getElementById('wth-thr').innerHTML = data.forecast.forecastday[3].day.condition.text;
            document.getElementById('temp-thr').innerHTML = `${data.forecast.forecastday[3].day.avgtemp_f}${String.fromCharCode(176)} F`;
                });
            }
    }); 


});